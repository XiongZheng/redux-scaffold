import React,{Fragment,Component} from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {setName} from "../actions/nameActions"

class Name extends Component{
  handleInputName(event) {
    const name = event.target.value;

    this.props.setName(name);
  }

  render() {

    const {name}=this.props.nameReducer;

    return (
      <Fragment>
        <input onChange={this.handleInputName.bind(this)} type="text"/>
        <p>Hello,{name}!</p>
      </Fragment>
    );
  }
}

const mapDispatchToProps=(dispatch)=>bindActionCreators(
  {setName},dispatch
);

const mapStateToProps=(state)=>({
  nameReducer: state.nameReducer
});


export default connect(mapStateToProps,mapDispatchToProps)(Name);