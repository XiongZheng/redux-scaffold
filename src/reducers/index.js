import {combineReducers} from "redux";
import nameReducer from "./NameReducer";

export default combineReducers({
  nameReducer
});