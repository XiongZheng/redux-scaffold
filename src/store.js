import {applyMiddleware,createStore} from "redux";
import thunk from "redux-thunk";
import rootRender from './reducers';

const configStore=(initialState)=>createStore(
  rootRender,
  initialState,
  applyMiddleware(thunk)
);

export  default configStore({});